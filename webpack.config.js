var path = require('path');


module.exports = {
    entry: ['babel-polyfill', './src/index.js'],
    devtool: 'eval-source-map',
    output: {
        path: __dirname + "/dist",
        publicPath: '/',
        filename: 'bundle.js'
    },

    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            Style: path.resolve(__dirname, 'src/style/'),
            Actions: path.resolve(__dirname, 'src/js/actions/'),
            Reducers: path.resolve(__dirname, 'src/js/reducers/'),
            Helpers: path.resolve(__dirname, 'src/js/helpers/'),
        }
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
            },
            {
                test: /\.jsx$/,
                loader: 'babel-loader',
            },
            {
                test: /\.png$/,
                loader: "file-loader",
            },
            {
                test: /\.(png|svg|jpg|gif|mp3)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                    }
                ]
            },

            {
                test: /\.(scss)$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: "[name]_[local]_[hash:base32]",
                            sourceMap: true,
                            minimize: true
                        }
                    },
                    {
                        loader: "sass-loader",
                    }
                ]
            }
        ],
    },

    devServer: {
        compress: true,
        port: 8080,
    },
}

