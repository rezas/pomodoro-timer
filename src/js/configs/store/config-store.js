import {createStore, compose, applyMiddleware} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {composeWithDevTools} from 'redux-devtools-extension/logOnlyInProduction'
import Reducers from '../../reducers/index'
import Sagas from '../../sagas/index'
import initialState from './initial-state'

const sagaMiddleware = createSagaMiddleware()
const middlewares = [
    sagaMiddleware,
]
const composeEnhancers = composeWithDevTools(
    applyMiddleware(...middlewares),
)

const store = createStore(Reducers,
    initialState,
    composeEnhancers)

//sagaMiddleware.run(Sagas.bind(null, store))
sagaMiddleware.run(Sagas)

export default store
