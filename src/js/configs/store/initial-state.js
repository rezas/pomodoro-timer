export default {
    clock: {
        currentState: 'session',
        setting: {
            session: {
                duration: 25,
                endSound: true,
                tickingSound: true,
            },
            break: {
                duration: 10,
                endSound: true,
                tickingSound: false,
            },
        }
    },
    tasks: [],
}
