import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import moment from 'moment'
import momentDurationFormatSetup from 'moment-duration-format'


import warningSoundTrack from '../../asset/warning.mp3'
import timeupSoundTrack from '../../asset/timeup.mp3'
import {
    sessionEnded,
    breakEnded,
} from 'Actions/clock-actions'
import ProgressbarView from '../components/progressbarView'
import Style from 'Style/clock-view.scss'

momentDurationFormatSetup(moment)

class ClockView extends Component {
    constructor() {
        super()
        this.onClick = this.onClick.bind(this)
        this.startTimer = this.startTimer.bind(this)
        this.stopTimer = this.stopTimer.bind(this)
        this.tick = this.tick.bind(this)
        this.reset = this.reset.bind(this)
        this.state = {
            isPaused: true,
            timer: 0,
            isSession: true,
        }
    }

    tick() {
        this.setState({timer: this.state.timer + 1},
            this.processTime,
        );
    }

    startTimer() {
        this.interval = setInterval(this.tick, 1000);
        this.setState({paused: false});
    }

    stopTimer() {
        clearInterval(this.interval);
        this.setState({paused: true});
    }

    reset() {
        this.setState({timer: 0, paused: true});
        clearInterval(this.interval);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    processTime() {
        const timer = this.state.timer
        const currentState = this.props.clockState
        const clockSetting = this.props.clockSetting
        const session = clockSetting[currentState].duration * 60
        const isSession = this.state.isSession
        if (session - timer < 60 && session - timer > 57) {
            if (clockSetting[currentState].tickingSound) {
                const warningSound = new Audio(warningSoundTrack)
                warningSound.play()
            }
        }

        if (session - timer === 0) {
            if (clockSetting[currentState].endSound) {
                const endSound = new Audio(timeupSoundTrack)
                endSound.play()
            }

            this.setState({
                isSession: !isSession,
                timer: 0,
            })

            if (isSession) {
                this.props.sessionEnded()
            } else {
                this.props.breakEnded()
            }
        }
    }

    onClick() {
        this.setState({
                isPaused: !this.state.isPaused,
            },
            () => {
                this.state.isPaused ?
                    this.stopTimer() :
                    this.startTimer()
            })

    }

    render() {
        const timer = this.state.timer
        const session = this.props.clockSetting[this.props.clockState].duration * 60
        const percentage = (timer * 100 / session)
        const remainingTime = moment.duration((session - timer), "second").format("mm:ss")
        return (
            <ProgressbarView
                isPaused={this.state.isPaused}
                isSession={this.state.isSession}
                onClick={this.onClick}
                remainingTime={remainingTime}
                percentage={percentage}
            />
        )
    }
}

const mapStateToProps = createStructuredSelector({
    clockSetting: ({clock}) => clock.setting,
    clockState: ({clock}) => clock.currentState,
})

const mapDispatchToProps = (dispatch) => {
    return {
        sessionEnded: () => dispatch(sessionEnded()),
        breakEnded: () => dispatch(breakEnded()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClockView)