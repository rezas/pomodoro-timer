import React, {Component} from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from "reselect";
import moment from 'moment'
import DatePicker from 'react-datepicker'
import {addNewTask} from 'Actions/task-actions'

import 'react-datepicker/dist/react-datepicker-cssmodules.css'
import Style from 'Style/task-form.scss'

class TaskForm extends Component {
    constructor() {
        super()
        this.handleCalenderChange = this.handleCalenderChange.bind(this)
        this.onInputChange = this.onInputChange.bind(this)
        this.onSubmitForm = this.onSubmitForm.bind(this)
        this.state = {
            selectedDate: moment().startOf('day'),
            title: ''
        }
    }

    handleCalenderChange(date) {
        this.setState({
            selectedDate: date,
        })
    }

    onSubmitForm(e) {
        e.preventDefault()
        this.props.addNewTask({
            date: this.state.selectedDate,
            title: this.state.title,
        })
        this.setState({
            title: ''
        })
    }

    onInputChange(e) {
        this.setState({
            title: e.target.value,
        })
    }

    render() {
        return (
            <form onSubmitCapture={this.onSubmitForm}>

                <div className={Style.container}>
                    <input
                        className={Style.input}
                        value={this.state.title}
                        type="text"
                        onChangeCapture={this.onInputChange}
                        placeholder="Title"
                    />
                    <DatePicker
                        selected={this.state.selectedDate}
                        onChange={this.handleCalenderChange}
                        minDate={moment()}
                        maxDate={moment().add(12, "months")}
                        showDisabledMonthNavigation
                    />
                    <button type="submit" className={Style.submitBtn}>Add</button>
                </div>
            </form>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addNewTask: ({date, title}) => dispatch(addNewTask({date, title})),

    }
}

export default connect(null, mapDispatchToProps)(TaskForm)