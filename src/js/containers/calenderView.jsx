import React, {Component} from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from "reselect"
import moment from 'moment'
import Day from '../components/day'
import 'bulma/css/bulma.css'
import Style from 'Style/calender-view.scss'

class Calender extends Component {
    constructor() {
        super()
    }

    renderDay({daysFromToday}) {
        const date = moment(new Date()).add(daysFromToday, 'days').startOf('day');
        const dateTasks = this.props.tasks.filter((task) => {
            return task.date.isSame(date)
        })
        return (
            <Day
                date={date}
                tasks={dateTasks}
            />
        )
    }

    render() {

        return (
            <div className={`card  ${Style.container}`}>
                <div className={`card-header ${Style.today}`}>
                    Today is {moment().format("MMM Do YY")}
                </div>
                <div className={`columns  ${Style.tasks}`}>
                    {this.renderDay({daysFromToday: -1})}
                    {this.renderDay({daysFromToday: 0})}
                    {this.renderDay({daysFromToday: 1})}
                    {this.renderDay({daysFromToday: 2})}
                </div>

            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    tasks: ({tasks}) => tasks,
})

const mapDispatchToProps = (dispatch) => {
    return {
        /*sessionEnded: () => dispatch(sessionEnded()),
        breakEnded: () => dispatch(breakEnded()),*/
    }
}

export default connect(mapStateToProps)(Calender)