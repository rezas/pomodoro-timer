import {handleActions} from 'redux-actions'
import * as actions from 'Actions/clock-actions'
import initialState from '../configs/store/initial-state'

export default handleActions({
    [actions.sessionEnded]: (state) => {
        return {
            ...state,
            currentState: 'break',
        }
    },
    [actions.breakEnded]: (state) => {
        return {
            ...state,
            currentState: 'session',
        }
    },

}, initialState)
