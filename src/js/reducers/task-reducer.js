import {handleActions} from 'redux-actions'
import update from 'immutability-helper'
import * as actions from 'Actions/task-actions'
import initialState from '../configs/store/initial-state'

export default handleActions({
    [actions.addNewTask]: (state = [], {payload}) => {
        const id = getNewID(state)
        return [
            ...state,
            {
                id,
                title: payload.title,
                date: payload.date,
                isCompleted: false,
            }
        ]

    },
    [actions.removeTask]: (state, {payload}) => {
        const oldState = [].concat(state)
        const taskIndex = oldState.findIndex((task) => task.id === payload.taskID)
        oldState.splice(taskIndex, 1)
        return [
            ...oldState,
        ]
    },
    [actions.changeTaskStatus]: (state, {payload}) => {
        const taskIndex = state.findIndex((task) => task.id === payload.taskID)
        const newState = update(state, {
            [taskIndex]: task => update(task, {
                isCompleted: isCompleted => update(isCompleted, {
                    $set: !isCompleted
                })
            })
        })

        return [
            ...newState
        ]
    }

}, initialState)

function getNewID(state) {
    if (!state.length) {
        return 1
    }
    return state.reduce((max, st) => {
        if (max.id < st.id) return st
        return max
    }).id + 1
}