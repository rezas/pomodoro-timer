import {createSelector} from 'reselect'

export function clockSettingSelector() {
    return createSelector((clock) => clock)
}