import React, {Component} from 'react'
import {connect} from 'react-redux'
import {removeTask, changeTaskStatus} from 'Actions/task-actions'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faTrash} from '@fortawesome/fontawesome-free-solid'
import Style from 'Style/task-view.scss'

class TaskView extends Component {
    constructor() {
        super()
        this.onClickDone = this.onClickDone.bind(this)
        this.onRemove = this.onRemove.bind(this)
    }

    onClickDone(id) {
        this.props.changeTaskStatus(id)
    }

    onRemove(id) {
        this.props.removeTask(id)
    }

    render() {
        const taskStatus = this.props.isCompleted ? 'complete' : 'notComplete'
        return (
            <div className={`${Style.container} ${Style[taskStatus]}`}>
                <div
                    className={Style.deleteIcon}
                    onClick={() => this.onRemove(this.props.id)}
                >
                    <FontAwesomeIcon
                        icon={faTrash}
                        color="red"
                    />
                </div>
                <div
                    className={Style.title}
                    onClick={() => this.onClickDone(this.props.id)}
                >
                    {this.props.title}
                </div>


            </div>

        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        removeTask: (id) => dispatch(removeTask({taskID: id})),
        changeTaskStatus: (id) => dispatch(changeTaskStatus({taskID: id}))
    }
}

export default connect(null, mapDispatchToProps)(TaskView)