import React from 'react'
import PropTypes from 'prop-types'
import Style from 'Style/progressbar.scss'

export default function Progressbar(props) {
    const sqSize = props.sqSize;
    const radius = (props.sqSize - props.strokeWidth) / 2;
    const viewBox = `0 0 ${sqSize} ${sqSize}`;
    const dashArray = radius * Math.PI * 2;
    const dashOffset = dashArray - dashArray * props.percentage / 100;

    return (
        <svg
            width={props.sqSize}
            height={props.sqSize}
            viewBox={viewBox}>
            <circle
                className={Style.circleBackground}
                cx={props.sqSize / 2}
                cy={props.sqSize / 2}
                r={radius}
                strokeWidth={`${props.strokeWidth}px`}/>
            <circle
                className={`${Style.circleProgress} ${Style[props.type]}`}
                cx={props.sqSize / 2}
                cy={props.sqSize / 2}
                r={radius}
                strokeWidth={`${props.strokeWidth}px`}
                transform={`rotate(-90 ${props.sqSize / 2} ${props.sqSize / 2})`}
                style={{
                    strokeDasharray: dashArray,
                    strokeDashoffset: dashOffset
                }}/>
            <text
                className={`${Style.circleText} ${Style[props.type]}`}
                x="50%"
                y="50%"
                dy=".3em"
                textAnchor="middle">
                {`${props.remainingTime}`}
            </text>
        </svg>
    );

}

Progressbar.defaultProps = {
    sqSize: 200,
    percentage: 0,
    strokeWidth: 10,
    remainingTime: '00:00',
    type: 'progress',
};

Progressbar.propTypes = {
    sqSize: PropTypes.number,
    percentage: PropTypes.number,
    strokeWidth: PropTypes.number,
    remainingTime: PropTypes.string,
    type: PropTypes.oneOf([
        'progress',
        'break',
    ])
};