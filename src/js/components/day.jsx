import React from 'react'
import TaskView from './task-view'
import 'bulma/css/bulma.css'
import Style from 'Style/day.scss'

export default function Day(props) {
    const taskView = getTasks(props.tasks)
    return (
        <div className={`column  ${Style.container}`}>
            <div>
                {props.date.format("MMM Do YY")}
            </div>
            <div>
                {taskView}
            </div>
        </div>
    )
}

function getTasks(tasks) {
    if (!tasks || !tasks.length) {
        return null
    }
    return tasks.map((task) => {
        return (
            <TaskView
                key={task.id}
                id={task.id}
                title={task.title}
                isCompleted={task.isCompleted}
            />
        )
    })
}