import React, {Component} from 'react'
import ClockView from '../containers/clockView'
import TaskForm from '../containers/taskForm'
import CalenderView from '../containers/calenderView'
import 'Style/app.scss'

export default class Pomodoro extends Component {
    render() {
        return (
            <div>
                <ClockView/>
                <TaskForm/>
                <CalenderView/>
            </div>
        )
    }
}