import React from 'react'
import Progressbar from './progressbar'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faPlay, faPause} from '@fortawesome/fontawesome-free-solid'

import Style from 'Style/progressbar-view.scss'

export default function ProgressbarView(props) {
    const icon = props.isPaused ? faPlay : faPause
    const type = props.isSession ? 'progress' : 'break'
    return (
        <div className={Style.container}>
            <Progressbar
                strokeWidth={10}
                sqSize={200}
                percentage={props.percentage}
                remainingTime={props.remainingTime}
                type={type}
            />
            <span
                className={Style.playIcon}
                onClick={() => onClick(props)}
            >
                <FontAwesomeIcon
                    icon={icon}
                    color="#24C3CE"
                    size="2x"
                />
                </span>
        </div>
    )
}

function onClick(props) {
    props.onClick()
}

ProgressbarView.defaultProps = {
    sqSize: 200,
    percentage: 0,
    strokeWidth: 10,
    isPaused: true
};

