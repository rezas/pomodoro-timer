import ReduxNamespacedActions from 'redux-namespaced-actions'

const { createAction } = ReduxNamespacedActions('clock')

export const sessionEnded = createAction('SESSION_Ended')
export const breakEnded = createAction('BREAK_Ended')
export const resetTimer = createAction('RESET_TIMER')
export const setBreakLength = createAction('SET_BREAK_LENGTH')
export const setSessionLength = createAction('SET_SESSION_LENGTH')