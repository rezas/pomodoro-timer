import ReduxNamespacedActions from 'redux-namespaced-actions'

const { createAction } = ReduxNamespacedActions('tasks')

export const addNewTask = createAction('ADD_NEW_TASK')
export const removeTask = createAction('REMOVE_TASK')
export const changeTaskStatus = createAction('CHANGE_TASK_STATUS')

