import React, {Component} from 'react';
import {Provider} from 'react-redux'
import store from './configs/store/config-store'

import Pomodoro from './components/pomodoro'

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Pomodoro/>
            </Provider>
        );
    }
}
